# Logica

Test DaCodes

### Recursos

1. Instalar node modules

    ```
    $ npm install
    ```

2. Es necesario tener instalado el Angular CLI

    ```
    $ npm install -g @angular/cli
    ```

### Clonar el repositorio

Para comenzar, clonamos el proyecto en la máquina local:

```
$ https://mikebizne@bitbucket.org/mikebizne/logica.git
```

### Abrir y Ejecutar proyecto con la consola 

1. Abrir la consola en la raíz del proyecto clonado
2. Ejecutar el comando

    ```
    $ ng serve
    ```

3. Se abrirá el navegador predeterminado con el puerto por default para visualizar el proyecto

    `http://localhost:4200/`
