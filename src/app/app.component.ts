import {Component, ChangeDetectorRef, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  positionLetter = '';
  position1: number;
  position2: number;
  array: any = [
    {'x': 1, 'y': 1, 'direction': ''},
    {'x': 2, 'y': 2, 'direction': ''},
    {'x': 3, 'y': 1, 'direction': ''},
    {'x': 3, 'y': 3, 'direction': ''}
  ];

  constructor() {
  }

  ngOnInit() {
    this.getDirection()
  }

  getDirection() {
    for (let pos of this.array) {
      this.setDirection(pos.x, pos.y, pos.direction)
    }
  }


  setDirection(x, y, direction) {
    const x_impar = x % 2 == 1;
    const y_impar = y % 2 == 1;
    if (!y_impar && x > y) {
      this.positionLetter = 'U';
    } else if (x_impar && y >= x) {
      this.positionLetter = 'R'
    } else if (!x_impar && !y_impar && y >= x) {
      this.positionLetter = 'L'
    } else if (x > y && x_impar && y_impar) {
      this.positionLetter = 'D'
    } else {
      this.positionLetter = "~:" + x + "x" + y
    }
    for (let pos of this.array) {
      if (pos.x == x && pos.y == y) {
        pos.direction = this.positionLetter;
      }
    }

  }

  add() {
    if (this.validInput()) {
      this.array.push(
        {'x': this.position1, 'y': this.position2, 'direction': ''}
      );
      this.getDirection();
    } else {
      alert('Type Position1 & Position2');
    }

  }

  validInput() {
    if (this.position1 && this.position2) {
      return true
    }
    return false;
  }
}
